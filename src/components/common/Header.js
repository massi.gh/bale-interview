import React from "react";

const Header = ({ title }) => {
  return (
    <div className="w-full sm:w-8/12 md:w-7/12 lg:w-7/12 xl:w-8/12 bg-green-400 h-24 fixed top-0 flex justify-center items-center shadow-lg rounded-br-2xl rounded-bl-2xl">
      <p className="text-2xl">{title}</p>
    </div>
  );
};

export default Header;
