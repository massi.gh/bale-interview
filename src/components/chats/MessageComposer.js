import React, { useState } from "react";
import { FiSend } from "react-icons/fi";

const MessageComposer = ({ onSendMessage }) => {
  //states
  const [message, setMessage] = useState("");

  const onClickSend = () => {
    onSendMessage(message);
    setMessage("");
  };

  return (
    <div
      style={{ boxShadow: "0 0 10px #aaa" }}
      className="flex flex-row-reverse h-16 fixed bottom-0 w-full sm:w-8/12 md:w-7/12 lg:w-7/12 xl:w-8/12 rounded-tr-2xl rounded-tl-2xl"
    >
      <button
        onClick={onClickSend}
        className="bg-green-400 w-24 flex items-center justify-center rounded-tr-2xl "
      >
        <FiSend size={30} className="text-white" />
      </button>
      <input
        value={message}
        onChange={(val) => setMessage(val.target.value)}
        className="w-full text-right px-2 rounded-tl-2xl"
        placeholder="پیام خود را وارد کنید"
      />
    </div>
  );
};

export default MessageComposer;
