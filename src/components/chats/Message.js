import React from "react";

const Message = ({ data, isUser }) => {
  return (
    <div
      className={`${
        isUser ? "bg-green-100" : "bg-yellow-100"
      } rounded-md p-3 mb-3 max-w-xl mx-4`}
    >
      <p className="text-lg">{data?.content}</p>
    </div>
  );
};

export default Message;
