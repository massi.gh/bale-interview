import { combineReducers } from "redux";
import chat from "./Chat/reducer";

export default combineReducers({ chat });
