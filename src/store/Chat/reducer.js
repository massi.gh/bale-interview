import { CHAT } from "./actions";

const initialState = {
  chat_data: {
    data: [
      {
        content: "hi bale",
        userId: 1,
      },
      {
        content: "you are the best",
        userId: 2484,
      },
      {
        content: "Looks like I should have used socket.io",
        userId: 2484,
      },
      {
        content: "But I did not have enough time to learn",
        userId: 2484,
      },
      {
        content: "I wish you good moments",
        userId: 1,
      },
      {
        content: "It was a good challenge",
        userId: 2484,
      },
      {
        content: "This is a test message",
        userId: 2484,
      },
      {
        content: "How are you",
        userId: 2484,
      },
      {
        content: "How are you",
        userId: 2484,
      },
      {
        content: "How are you",
        userId: 2484,
      },
      {
        content: "How are you",
        userId: 2484,
      },
      {
        content: "How are you",
        userId: 2484,
      },
    ],
    current_page: 1,
    last_page: 2,
  },
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case CHAT.SEND_MESSAGE:
      return {
        ...state,
        chat_data: {
          data: [...state.chat_data.data, action.payload],
        },
      };
    case CHAT.GET_NEXT_PAGE:
      return {
        ...state,
        chat_data: {
          data: [action.payload, ...state.chat_data.data],
        },
      };
    default:
      return state;
  }
}
export default reducer;
