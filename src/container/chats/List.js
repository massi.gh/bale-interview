import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Message, MessageComposer, Header } from "components";
import { CHAT } from "store/Chat/actions";
import { InView } from "react-intersection-observer";

const List = () => {
  //dispatcher
  const dispatch = useDispatch();
  //constants
  const userId = 2484;
  //select state store
  const chatList = useSelector((state) => state.chat.chat_data);

  const registerMessage = (message) => {
    const payload = { content: message, userId: userId };
    dispatch({ type: CHAT.SEND_MESSAGE, payload });
    window.scrollTo({ top: document.body.scrollHeight, behavior: "smooth" });
  };
  const loadMoreChats = (isInView) => {
    if (!isInView) return;
    const payload = { content: "page2", userId: 1 };
    dispatch({ type: CHAT.GET_NEXT_PAGE, payload });
  };
  const ListFooterComponent = () => {
    if (chatList.current_page < chatList.last_page)
      return (
        <InView
          className="m-auto h-16 flex justify-center flex-center"
          as="div"
          onChange={(inView, entry) => loadMoreChats(inView)}
        >
          <p>Loading...</p>
        </InView>
      );
  };
  useEffect(() => {
    window.scrollTo({ top: document.body.scrollHeight, behavior: "smooth" });
  }, []);
  return (
    <div className="flex items-center justify-center">
      <Header title="Justin Olson" />
      <div
        className="w-full sm:w-8/12 md:w-7/12 lg:w-7/12 xl:w-8/12 flex flex-col"
        style={{
          marginTop: "7rem",
          marginBottom: "7rem",
        }}
      >
        {ListFooterComponent()}
        {chatList?.data?.map((item, index) => {
          const isUser = item?.userId === userId;
          return (
            <div
              key={index}
              className={`w-full flex ${
                isUser ? "justify-end" : "justify-start"
              }`}
            >
              <Message data={item} isUser={isUser} />
            </div>
          );
        })}
      </div>
      <MessageComposer onSendMessage={registerMessage} />
    </div>
  );
};

export default List;
