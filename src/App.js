import "./App.css";
import { Provider } from "react-redux";
import store from "./store/configureStore";
import { List } from "container";
function App() {
  return (
    <Provider store={store}>
      <List />
    </Provider>
  );
}

export default App;
